/*
 * Copyright (c) 2016 John Krause.
 *
 * This file is part of DigiTalk.
 *
 *     DigiTalk is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DigiTalk is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tk.jpkrause.digitalk.protocol.layer.transport;

import android.util.Log;

import java.nio.ByteBuffer;
import java.util.TreeMap;

import tk.jpkrause.digitalk.protocol.layer.datalink.DataLinkLayer;

public class Server implements TimeoutHandler.Callback, DataLinkLayer.Listener {
	public interface Listener {
		void onError(Exception e);
		void onTimeout(Exception e);
		void onFinished(TransportMessage message);
	}

	private enum State {
		IDLE,
		AWAITING_ORDERS,
		FINISHED
	}

	private volatile Server.State currentState, nextState;
	private Server.Listener listener;
	private TransportPacketFactory transportPacketFactory;
	private TransportPacket currentTransportPacket;
	private TimeoutHandler timeoutHandler;
	private DataLinkLayer dataLinkLayer;
	private long timeout;
	private int numRetries, maxRetries;
	private TreeMap<Byte, byte[]> receivedData;
	private byte sourceId;

	public static class Builder {

		Server server;

		public Builder() {
			server = new Server();
		}

		public Server.Builder setListener(Server.Listener listener) {
			server.listener = listener;
			return this;
		}

		public Server.Builder setDataLinkLayer(DataLinkLayer dataLinkLayer) {
			server.dataLinkLayer = dataLinkLayer;
			return this;
		}

		public Server.Builder setMessageFactory(TransportPacketFactory transportPacketFactory) {
			server.transportPacketFactory = transportPacketFactory;
			return this;
		}

		public Server.Builder setTimeout(long timeout) {
			server.timeout = timeout;
			return this;
		}

		public Server.Builder setTimeoutHandler(TimeoutHandler timeoutHandler) {
			server.timeoutHandler = timeoutHandler;
			return this;
		}

		public Server.Builder setMaxRetries(int maxRetries) {
			server.maxRetries = maxRetries;
			return this;
		}

		public Server build() {
			server.receivedData = new TreeMap<>();
			server.dataLinkLayer.addListener(server);
			server.reset();
			return server;
		}
	}

	private void reset() {
		transportPacketFactory.initSequenceNumber();
		stopTimeout();
		setCurrentState(State.IDLE);
		setNextState(null);
		currentTransportPacket = null;
		receivedData.clear();
	}

	@Override
	public void onTransmitComplete() {
		if(nextState != null) {
			setCurrentState(nextState);
			startTimeout();
		}
	}

	@Override
	public void onReceiveStart() {
		timeoutHandler.pause();
	}

	@Override
	public void onReceiveError(Exception e) {
		Log.d(Server.class.getSimpleName(), "Error on receive");
		timeoutHandler.resume();
	}

	@Override
	public void onReceive(byte[] data) {
		TransportPacket packet = TransportPacket.fromBytes(data);
		if(packet != null && validateMessage(packet)) {
			Log.d(Server.class.getSimpleName(), "RECEIVED: " + packet.toString());
			if((currentTransportPacket == null
					|| packet.getAcknowledgmentNumber() == currentTransportPacket.getSequenceNumber())) {
				if(!onReceive(packet)) {
					sendCurrentMessage();
				}
			} else {
				sendCurrentMessage();
			}
		} else {
			timeoutHandler.resume();
		}
	}

	private void sendAwaitingOrders(byte sourceId, byte destinationId, byte acknowledgmentNumber) {
		currentTransportPacket = transportPacketFactory.awaitingOrders(sourceId, destinationId, acknowledgmentNumber);
		sendNewMessage();
	}

	private void sendAcknowledged(byte sourceId, byte destinationId, byte acknowledgmentNumber) {
		currentTransportPacket = transportPacketFactory.acknowledge(sourceId, destinationId, acknowledgmentNumber);
		sendNewMessage();
	}

	private void sendFinished(byte sourceId, byte destinationId, byte acknowledgmentNumber) {
		currentTransportPacket = transportPacketFactory.finished(sourceId, destinationId, acknowledgmentNumber);
		sendNewMessage();
	}

	private void startTimeout() {
		if(shouldTimeout()) {
			timeoutHandler.start(timeout, this);
		}
	}

	private void stopTimeout() {
		timeoutHandler.stop();
	}

	private void resendCurrentMessage() {
		numRetries++;
		sendCurrentMessage();
	}

	private void sendNewMessage() {
		numRetries = 0;
		sendCurrentMessage();
	}

	private void sendCurrentMessage() {
		if(currentTransportPacket != null) {
			Log.d(Server.class.getSimpleName(), "SENDING: " + currentTransportPacket.toString());
			dataLinkLayer.transmit(currentTransportPacket.compile());
		} else {
			//TODO
			Log.e(Server.class.getSimpleName(), "Error on send");
			listener.onError(new Exception());
		}
	}

	private boolean onReceive(TransportPacket transportPacket) {
		switch (currentState) {
			case IDLE:
				return handleIdle(transportPacket);
			case AWAITING_ORDERS:
				return handleAwaitingOrders(transportPacket);
			case FINISHED:
				return handleFinished(transportPacket);
			default:
				return false;
		}
	}

	private boolean handleFinished(TransportPacket transportPacket) {
		if(transportPacket.getType().equals(TransportPacket.Type.ACKNOWLEDGE)) {
			TransportMessage message = new TransportMessage();
			message.setData(getProcessedData());
			message.setRecipientId(transportPacket.getDestinationId());
			message.setSenderId(transportPacket.getSourceId());
			listener.onFinished(message);
			reset();
			return true;
		}
		return false;
	}

	private boolean handleAwaitingOrders(TransportPacket transportPacket) {
		if(transportPacket.getType().equals(TransportPacket.Type.DATA)) {
			processData(transportPacket.getSequenceNumber(), transportPacket.getData());
			sendAcknowledged(transportPacket.getDestinationId(), transportPacket.getSourceId(), transportPacket.getSequenceNumber());
			return true;
		} else if(transportPacket.getType().equals(TransportPacket.Type.FINISHED)) {
			setNextState(State.FINISHED);
			sendFinished(transportPacket.getDestinationId(), transportPacket.getSourceId(), transportPacket.getSequenceNumber());
			return true;
		}
		return false;
	}

	private boolean handleIdle(TransportPacket transportPacket) {
		if(transportPacket.getType().equals(TransportPacket.Type.SUMMON)) {
			setNextState(State.AWAITING_ORDERS);
			sendAwaitingOrders(transportPacket.getDestinationId(), transportPacket.getSourceId(), transportPacket.getSequenceNumber());
			return true;
		}
		return false;
	}

	private boolean validateMessage(TransportPacket transportPacket) {
		return transportPacket.isValid() && transportPacket.getDestinationId() == sourceId;
	}

	private byte[] getProcessedData() {
		ByteBuffer byteBuffer = ByteBuffer.allocate(receivedData.size()*TransportPacket.MAX_DATA_BYTES);
		for(Byte sequenceNumber : receivedData.navigableKeySet()) {
			byteBuffer.put(receivedData.get(sequenceNumber));
		}
		return byteBuffer.array();
	}

	private void processData(Byte sequenceNumber, byte[] data) {
		if(!receivedData.containsKey(sequenceNumber)) {
			receivedData.put(sequenceNumber, data);
		}
	}

	@Override
	public void onTimeout() {
		if(shouldTimeout()) {
			if (numRetries >= maxRetries) {
				if(timeoutCausesError()) {
					listener.onTimeout(new Exception(String.format("Timed out in state %s", currentState.name())));
				} else {
					TransportMessage message = new TransportMessage();
					message.setData(getProcessedData());
					message.setRecipientId(currentTransportPacket.getSourceId());
					message.setSenderId(currentTransportPacket.getDestinationId());
					listener.onFinished(message);
				}
				reset();
			} else {
				resendCurrentMessage();
			}
		}
	}

	private boolean timeoutCausesError() {
		return currentState == State.AWAITING_ORDERS;
	}

	private boolean shouldTimeout() {
		return currentState != State.IDLE;
	}

	public void setSourceId(byte id) {
		this.sourceId = id;
	}

	private void setCurrentState(State currentState) {
		Log.d(Server.class.getSimpleName(), "setCurrentState: " + currentState);
		this.currentState = currentState;
	}

	private void setNextState(State nextState) {
		Log.d(Server.class.getSimpleName(), "setNextState: " + nextState);
		this.nextState = nextState;
	}
}
