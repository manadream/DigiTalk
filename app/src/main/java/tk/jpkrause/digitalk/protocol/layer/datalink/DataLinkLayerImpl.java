/*
 * Copyright (c) 2016 John Krause.
 *
 * This file is part of DigiTalk.
 *
 *     DigiTalk is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DigiTalk is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tk.jpkrause.digitalk.protocol.layer.datalink;

import android.util.Log;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import tk.jpkrause.digitalk.protocol.layer.physical.PhysicalLayer;

public class DataLinkLayerImpl implements DataLinkLayer, PhysicalLayer.Listener {

	private PhysicalLayer physicalLayer;
	private List<Listener> listeners;
	private List<DataLinkFrame> framesInTransmission;
	private int framesTransmitted;
	private static final ByteBuffer FRAME_BUFFER = ByteBuffer.allocateDirect(
			DataLinkFrame.PREAMBLE.length + DataLinkFrame.MAX_FRAME_SIZE_BYTES);
	private Executor onReceiveStartExecutor;
	private Runnable onReceiveStartRunnable = new Runnable() {
		@Override
		public void run() {
			for(Listener listener : listeners) {
				listener.onReceiveStart();
			}
		}
	};

	public DataLinkLayerImpl(PhysicalLayer physicalLayer) {
		listeners = new LinkedList<>();
		this.physicalLayer = physicalLayer;
		physicalLayer.setListener(this);
		onReceiveStartExecutor = Executors.newSingleThreadExecutor();
	}

	@Override
	public void transmit(byte[] data) {
		//TODO add empty check and throw or return something to indicate failure
		framesInTransmission = DataLinkFrame.createFramesFromBytes(data);
		framesTransmitted = 0;
		if(framesInTransmission != null && !framesInTransmission.isEmpty()) {
			for (DataLinkFrame frame : framesInTransmission) {
				physicalLayer.transmit(frame.wrapAndSerialize());
			}
		}
	}

	@Override
	public void addListener(Listener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeListener(Listener listener) {
		listeners.remove(listener);
	}

	@Override
	public void onTransmitComplete() {
		framesTransmitted++;
		if(framesTransmitted == framesInTransmission.size()) {
			for(Listener listener : listeners) {
				listener.onTransmitComplete();
			}
		}
	}

	@Override
	public void onReceive(byte data) {
		synchronized (FRAME_BUFFER) {
			if(FRAME_BUFFER.position() == 0) {
				onReceiveStartExecutor.execute(onReceiveStartRunnable);
			}
			if (FRAME_BUFFER.hasRemaining()) {
				FRAME_BUFFER.put(data);
			}
		}
	}

	@Override
	public void onReceiveComplete() {
		byte[] data;
		synchronized (FRAME_BUFFER) {
			FRAME_BUFFER.flip();
			data = new byte[FRAME_BUFFER.remaining()];
			FRAME_BUFFER.get(data);
			FRAME_BUFFER.clear();
		}
		DataLinkFrame dataLinkFrame = DataLinkFrame.unwrapAndDeserialize(data);
		if(dataLinkFrame != null && dataLinkFrame.isValid()) {
			for(Listener listener : listeners) {
				listener.onReceive(dataLinkFrame.getPayload());
			}
		} else {
			Exception exception = new Exception("Invalid Frame");
			for(Listener listener : listeners) {
				listener.onReceiveError(exception);
			}
		}
	}
}
