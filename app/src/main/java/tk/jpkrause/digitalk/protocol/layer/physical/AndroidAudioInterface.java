/*
 * Copyright (c) 2016 John Krause.
 *
 * This file is part of DigiTalk.
 *
 *     DigiTalk is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DigiTalk is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tk.jpkrause.digitalk.protocol.layer.physical;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.io.TarsosDSPAudioFormat;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;

public class AndroidAudioInterface implements AudioInterface, AudioTrack.OnPlaybackPositionUpdateListener {

	private AudioDispatcher audioDispatcher;
	private AudioTrack audioTrack;
	private ExecutorService executorService;
	private Listener listener;

	public AndroidAudioInterface(PhysicalLayerParams params) {
		int minBufferSize = AudioRecord.getMinBufferSize(params.getSampleRate(),
				AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
		int audioBufferSize = ((minBufferSize/params.getSamplesPerBit())+1)*params.getSamplesPerBit();
		audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
				params.getSampleRate(),
				AudioFormat.CHANNEL_OUT_MONO,
				AudioFormat.ENCODING_PCM_16BIT,
				audioBufferSize,
				AudioTrack.MODE_STREAM);
		audioTrack.setPlaybackPositionUpdateListener(this);
		audioDispatcher = AudioDispatcherFactory
				.fromDefaultMicrophone(params.getSampleRate(), audioBufferSize, 0);
		executorService = Executors.newSingleThreadExecutor();
	}

	@Override
	public void onMarkerReached(AudioTrack track) {
		if(listener != null) {
			listener.onTransmissionComplete();
		}
	}

	@Override
	public void onPeriodicNotification(AudioTrack track) {

	}

	@Override
	public void addAudioProcessor(AudioProcessor audioProcessor) {
		audioDispatcher.addAudioProcessor(audioProcessor);
	}

	@Override
	public void removeAudioProcessor(AudioProcessor audioProcessor) {
		audioDispatcher.removeAudioProcessor(audioProcessor);
	}

	@Override
	public void start() {
		audioTrack.play();
		executorService.execute(audioDispatcher);
	}

	@Override
	public void write(final short[] buffer) {
		audioTrack.setNotificationMarkerPosition(audioTrack.getPlaybackHeadPosition() + buffer.length);
		audioTrack.write(buffer, 0, buffer.length);
	}

	@Override
	public void stop() {
		audioTrack.stop();
		audioDispatcher.stop();
		executorService.shutdown();
	}

	@Override
	public TarsosDSPAudioFormat getAudioFormat() {
		return audioDispatcher.getFormat();
	}

	@Override
	public void setListener(Listener listener) {
		this.listener = listener;
	}
}
