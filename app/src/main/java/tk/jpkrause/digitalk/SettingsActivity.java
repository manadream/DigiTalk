/*
 * Copyright (c) 2016 John Krause.
 *
 * This file is part of DigiTalk.
 *
 *     DigiTalk is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DigiTalk is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tk.jpkrause.digitalk;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import tk.jpkrause.digitalk.protocol.layer.physical.PhysicalLayerParams;

public class SettingsActivity extends AppCompatActivity {

	EditText identifierField;
	EditText baudField;
	EditText zeroField;
	EditText oneField;
	SeekBar powerSlider;
	TextView powerValue;
	Button saveButton;
	Spinner paramsSpinner;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		initViews();
	}

	private void initViews() {
		identifierField = (EditText) findViewById(R.id.identifier_field);
		identifierField.setText((String.format(Locale.US, "%s", (char)SettingsManager.getInstance().getSourceId(this))));

		baudField = (EditText) findViewById(R.id.baud_field);
		zeroField = (EditText) findViewById(R.id.freq_zero_field);
		oneField = (EditText) findViewById(R.id.freq_one_field);
		powerSlider = (SeekBar) findViewById(R.id.power_slider);
		powerValue = (TextView) findViewById(R.id.power_value);
		saveButton = (Button) findViewById(R.id.save_button);
		saveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				saveParams();
				finish();
			}
		});

		powerSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				powerValue.setText(String.format(Locale.US, "%d", progress));
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});

		paramsSpinner = (Spinner) findViewById(R.id.preset_params_spinner);
		ArrayAdapter<PhysicalLayerParams> adapter =
				new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getSpinnerList());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		paramsSpinner.setAdapter(adapter);
		paramsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				updateFields((PhysicalLayerParams)parent.getItemAtPosition(position));
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	private List<PhysicalLayerParams> getSpinnerList() {
		ArrayList<PhysicalLayerParams> params = new ArrayList<>();
		params.add(SettingsManager.getInstance().getPhysicalLayerParams(this));
		params.addAll(PhysicalLayerParams.getAll());
		return params;
	}
	private void updateFields(PhysicalLayerParams params) {
		baudField.setText(String.format(Locale.US, "%d", params.getBaud()));
		zeroField.setText(String.format(Locale.US, "%.0f", params.getFrequencyZero()));
		oneField.setText((String.format(Locale.US, "%.0f", params.getFrequencyOne())));
		powerSlider.setProgress(params.getMinimumFrequencyPower());
	}

	private void saveParams() {
		if(fieldEmpty(identifierField)
				|| fieldEmpty(baudField)
				|| fieldEmpty(zeroField)
				|| fieldEmpty(oneField)) {
			Toast.makeText(this, R.string.empty_field_error, Toast.LENGTH_SHORT).show();
		} else {
			SettingsManager.getInstance().setSourceId(this, (byte)identifierField.getText().toString().charAt(0));
			SettingsManager.getInstance().setPhysicalLayerParams(this,
					PhysicalLayerParams.customParams(
							Integer.parseInt(baudField.getText().toString()),
							Double.parseDouble(zeroField.getText().toString()),
							Double.parseDouble(oneField.getText().toString()),
							powerSlider.getProgress()
					));
			Toast.makeText(this, R.string.prefs_saved, Toast.LENGTH_SHORT).show();
		}
	}

	private boolean fieldEmpty(EditText field) {
		return field.getText().length() <= 0;
	}
}
